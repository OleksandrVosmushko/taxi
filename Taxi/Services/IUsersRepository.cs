﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Taxi.Entities;

namespace Taxi.Services
{
    public interface IUsersRepository
    {
        Task AddCustomer(AppUser user);
    }
}
