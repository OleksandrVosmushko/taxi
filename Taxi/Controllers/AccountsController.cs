﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Taxi.Entities;
using Taxi.Models;
using Taxi.Services;

namespace Taxi.Controllers
{
    [Route ("[controller]")]
    public class AccountsController : Controller
    {
        private IMapper _mapper;
        private UserManager<AppUser> _userManager;
        private IUsersRepository _usersRepository;

        public AccountsController(UserManager<AppUser> userManager, IMapper mapper, IUsersRepository usersRepository)
        {
            _mapper = mapper;
            _userManager = userManager;
            _usersRepository = usersRepository;
        }

        [HttpPost]
        public async Task<IActionResult> RegisterCustomer([FromBody] CustomerRegistrationDto model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var userIdentity = _mapper.Map<AppUser>(model);

            var result = await _userManager.CreateAsync(userIdentity, model.Password);
            if (!result.Succeeded)
            {
                return BadRequest();
            }
            await _usersRepository.AddCustomer(userIdentity);

            return new OkObjectResult("Account created");
        }
    }
}
