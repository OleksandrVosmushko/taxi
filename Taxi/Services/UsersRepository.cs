﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Taxi.Data;
using Taxi.Entities;

namespace Taxi.Services
{
    public class UsersRepository : IUsersRepository
    {
        ApplicationDbContext _dataContext; 
        public UsersRepository(ApplicationDbContext dataContext)
        {
            _dataContext = dataContext;
        }
        public async Task AddCustomer(AppUser userIdentity)
        {
            await _dataContext.Customers.AddAsync(new Customer { IdentityId = userIdentity.Id});
            await _dataContext.SaveChangesAsync();
        }
    }
}
